module.exports = {
  semi: false,
  trailingComma: 'all', //all
  singleQuote: true,
  printWidth: 100,
  tabWidth: 2,
  'prettier/prettier': [
    'error',
    {
      'endOfLine': 'auto',
    }
  ]
}

