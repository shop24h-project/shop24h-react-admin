import { cilWatch, cilUser, cilBrush } from '@coreui/icons'
import CIcon from '@coreui/icons-react'
import { CNavItem, CNavTitle } from '@coreui/react'
import React from 'react'

const _nav = [
  {
    component: CNavItem,
    name: 'Clothes Shop',
    to: '/dashboard',
    icon: <CIcon icon={cilWatch} customClassName="nav-icon" />,
    badge: {
      color: 'info',
      text: 'NEW',
    },
  },
  {
    component: CNavTitle,
    name: 'Manage',
  },
  {
    component: CNavItem,
    name: 'Orders',
    to: '/orders',
    icon: <CIcon icon={cilBrush} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Products',
    to: '/products',
    icon: <CIcon icon={cilWatch} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Product Types',
    to: '/product-types',
    icon: <CIcon icon={cilWatch} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Customers',
    to: '/customers',
    icon: <CIcon icon={cilWatch} customClassName="nav-icon" />,
  },
]
export default _nav
