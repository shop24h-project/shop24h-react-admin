import React from 'react'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CRow,
  CTable,
  CTableBody,
  CTableDataCell,
  CTableHead,
  CTableHeaderCell,
  CTableRow,
} from '@coreui/react'
import {AppSidebar, AppFooter, AppHeader } from '../components/index'
import ProductTypeList from '../components/productType/ProductTypeList'
const Product = () => {
  return (
    <div>
      <AppSidebar />
      <div className="wrapper d-flex flex-column min-vh-100 bg-light">
        <AppHeader />
        <div className="body flex-grow-1 px-3">
          <ProductTypeList />
        </div>
        <AppFooter />
      </div>
    </div>
  )
}

export default Product
