import {
    COST_CHANGE,
    NOTE_CHANGE
} from "../constants/inputOrderChange";

const initialState = {
    cost: 0,
    note: "",
}

export default function inputOrderChangeReducer(state = initialState, action) {
    switch (action.type) {
        case COST_CHANGE:
            return {
                ...state,
                cost: action.payload
            }
        case NOTE_CHANGE:
            return {
                ...state,
                note: action.payload
            }
        default:
            return state;
    }
}

