import { CREATE_PRODUCT_TYPE, CREATE_PRODUCT_TYPE_ERROR, CREATE_PRODUCT_TYPE_PENDING } from "../constants/createProductType";

const initialState = {
  createProductType: {},
  pending: false,
  error: null
}

export default function createOrderReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_PRODUCT_TYPE_PENDING:
      return {
        ...state,
        pending: true
      };
    case CREATE_PRODUCT_TYPE:
      return {
        ...state,
        createProductType: action.payload
      }
    case CREATE_PRODUCT_TYPE_ERROR:
      return {
        ...state,
        error: action.error,
        pending: false
      };
    default:
      return state;
  }
}

