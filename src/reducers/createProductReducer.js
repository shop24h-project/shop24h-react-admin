import { CREATE_PRODUCT, CREATE_PRODUCT_ERROR, CREATE_PRODUCT_PENDING } from "../constants/createProduct";

const initialState = {
  product: {},
  pending: false,
  error: null
}

export default function createCustomerReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_PRODUCT_ERROR:
      return {
        ...state,
        pending: true
      };
    case CREATE_PRODUCT:
      return {
        ...state,
        product: action.payload
      }
    case CREATE_PRODUCT_PENDING:
      return {
        ...state,
        error: action.error,
        pending: false
      };
    default:
      return state;
  }
}

