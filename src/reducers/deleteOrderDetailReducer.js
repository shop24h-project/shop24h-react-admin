import { DELETE_ORDER_DETAIL_ERROR, DELETE_ORDER_DETAIL_SUCCESS } from "../constants/deleteOrderDetail";

const initialState = {
  currentOrderDetail: {},
  error: null,
}

export default function deleteOrderDetailReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_ORDER_DETAIL_SUCCESS:
      return {
        ...state,
        currentOrderDetail: action.payload
      }
    case DELETE_ORDER_DETAIL_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
