import { GET_ALL_ORDER, CHANGE_PAGE_PAGINATION} from "../constants/getOrder";

const limit = 6;

const initialState = {
    currentPage: 1,
    orders: [],
    countAll: 0,
    noPage: 0,
    orderInfo: "",
}

export default function getOrderReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_ORDER:
            return {
                ...state,
                countAll : action.data.length,
                orders: action.data.slice((state.currentPage - 1) * limit, state.currentPage * limit),
                noPage: Math.ceil(action.data.length / limit),
            }
        case CHANGE_PAGE_PAGINATION:
            return {
                ...state,
                currentPage: action.payload
            }
        default:
            return state;
    }
}

