import { EDIT_CUSTOMER_ERROR, EDIT_CUSTOMER_SUCCESS } from "../constants/editCustomer";

const initialState = {
  currentState: {},
  error: null,
}

export default function editCustomerReducer(state = initialState, action) {
  switch (action.type) {
    case EDIT_CUSTOMER_SUCCESS:
      return {
        ...state,
        currentState: action.payload
      }
    case EDIT_CUSTOMER_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
