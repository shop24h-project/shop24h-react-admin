import {
    NAME_CHANGE,
    DESCRIPTION_CHANGE,
    TYPE_CHANGE,
    IMAGE_URL_CHANGE,
    BUY_PRICE_CHANGE,
    PROMORTION_PRICE_CHANGE,
    AMOUNT_CHANGE,
    IS_POPULAR_CHANGE,
    COLOR_CHANGE,
    SUB_IMAGE_CHANGE
} from "../constants/inputProductChange";

const initialState = {
    name: "",
    description: "",
    type: "none",
    imageUrl: "",
    buyPrice: 0,
    promotionPrice: 0,
    amount: 0,
    isPopular: false,
    color: "red",
    subImage: "",
}

export default function inputProductChangeReducer(state = initialState, action) {
    switch (action.type) {
        case NAME_CHANGE:
            return {
                ...state,
                name: action.payload
            }
        case DESCRIPTION_CHANGE:
            return {
                ...state,
                description: action.payload
            }
        case TYPE_CHANGE:
            return {
                ...state,
                type: action.payload
            }
        case IMAGE_URL_CHANGE:
            return {
                ...state,
                imageUrl: action.payload
            }
        case BUY_PRICE_CHANGE:
            return {
                ...state,
                buyPrice: action.payload
            }
        case PROMORTION_PRICE_CHANGE:
            return {
                ...state,
                promotionPrice: action.payload
            }
        case AMOUNT_CHANGE:
            return {
                ...state,
                amount: action.payload
            }
        case IS_POPULAR_CHANGE:
            return {
                ...state,
                isPopular: action.payload
            }
        case COLOR_CHANGE:
            return {
                ...state,
                color: action.payload
            }
        case SUB_IMAGE_CHANGE:
            return {
                ...state,
                subImage: action.payload
            }
        default:
            return state;
    }
}

