import { DELETE_CUSTOMER_SUCCESS, DELETE_CUSTOMER_ERROR } from "../constants/deleteCustomer";

const initialState = {
  currentCustomer: {},
  error: null,
}

export default function deleteCustomerReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_CUSTOMER_SUCCESS:
      return {
        ...state,
        currentCustomer: action.payload
      }
    case DELETE_CUSTOMER_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
