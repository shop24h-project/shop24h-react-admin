import {
    NAME_CHANGE,
    IMAGE_URL_CHANGE,
    DESCRIPTION_CHANGE,
} from "../constants/inputChange";

const initialState = {
    name: "",
    imageUrl: "",
    description: "",
}

export default function inputChangeReducer(state = initialState, action) {
    switch (action.type) {
         case NAME_CHANGE:
              return {
                   ...state,
                   name: action.payload
              }
         case IMAGE_URL_CHANGE:
              return {
                   ...state,
                   imageUrl: action.payload
              }
         case DESCRIPTION_CHANGE:
              return {
                   ...state,
                   description: action.payload
              }
         default:
              return state;
    }
}

