import { combineReducers } from "redux";
import getProductReducer from "./getProductReducer";
import getProductTypeReducer from "./getProductTypeReducer";
import getCustomerReducer from "./getCustomerReducer";
import getOrderReducer from "./getOrderReducer";
import createProductTypeReducer from "./createProductTypeReducer";
import deleteProductTypeReducer from "./deleteProductTypeReducer";
import editProductTypeReducer from "./editProductTypeReducer";
import inputChangeReducer from "./inputChangeReducer";
import inputCustomerChangeReducer from "./inputCustomerChangeReducer";
import createCustomerReducer from "./createCustomerReducer";
import deleteCustomerReducer from "./deleteCustomerReducer";
import editCustomerReducer from "./editCustomerReducer";
import deleteProductReducer from "./deleteProductReducer";
import createProductReducer from "./createProductReducer";
import inputProductChangeReducer from "./inputProductChangeReducer";
import inputOrderChangeReducer from "./inputOrderChangeReducer";
import editProductReducer from "./editProductReducer";
import createOrderReducer from "./createOrderReducer";
import deleteOrderReducer from "./deleteOrderReducer";
import createOrderDetailReducer from "./createOrderDetailReducer";
import inputOrderDetailChangeReducer from "./inputOrderDetailChangeReducer";
import getOrderDetailReducer from "./getOrderDetailReducer";
import editOrderDetailReducer from "./editOrderDetailReducer";
import deleteOrderDetailReducer from "./deleteOrderDetailReducer";
const rootReducer = combineReducers({
    getProductReducer,
    getProductTypeReducer,
    getCustomerReducer,
    getOrderReducer,
    createProductTypeReducer,
    createOrderReducer,
    deleteProductTypeReducer,
    deleteCustomerReducer,
    editProductTypeReducer,
    inputChangeReducer,
    createCustomerReducer,
    inputCustomerChangeReducer,
    editCustomerReducer,
    deleteProductReducer,
    createProductReducer,
    inputProductChangeReducer,
    inputOrderChangeReducer,
    deleteOrderReducer,
    editProductReducer,
    createOrderDetailReducer,
    inputOrderDetailChangeReducer,
    getOrderDetailReducer,
    editOrderDetailReducer,
    deleteOrderDetailReducer
});

export default rootReducer;

