import { EDIT_PRODUCT_ERROR, EDIT_PRODUCT_SUCCESS } from "../constants/editProduct";

const initialState = {
  currentState: {},
  error: null,
}

export default function editProductReducer(state = initialState, action) {
  switch (action.type) {
    case EDIT_PRODUCT_SUCCESS:
      return {
        ...state,
        currentState: action.payload
      }
    case EDIT_PRODUCT_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
