import { DELETE_PRODUCT_TYPE_ERROR, DELETE_PRODUCT_TYPE_SUCCESS } from "../constants/deleteProductType";

const initialState = {
  currentProductType: {},
  error: null,
}

export default function deleteProductTypeReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_PRODUCT_TYPE_SUCCESS:
      return {
        ...state,
        currentProductType: action.payload
      }
    case DELETE_PRODUCT_TYPE_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
