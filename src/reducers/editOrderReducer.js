import { EDIT_ORDER_ERROR, EDIT_ORDER_SUCCESS } from "../constants/editOrder";

const initialState = {
  currentState: {},
  error: null,
}

export default function editOrderReducer(state = initialState, action) {
  switch (action.type) {
    case EDIT_ORDER_SUCCESS:
      return {
        ...state,
        currentState: action.payload
      }
    case EDIT_ORDER_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
