import { DELETE_PRODUCT_ERROR, DELETE_PRODUCT_SUCCESS } from "../constants/deleteProduct";

const initialState = {
  currentProduct: {},
  error: null,
}

export default function deleteProductReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_PRODUCT_SUCCESS:
      return {
        ...state,
        currentProduct: action.payload
      }
    case DELETE_PRODUCT_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
