import { CREATE_ORDER_DETAIL, CREATE_ORDER_DETAIL_ERROR, CREATE_ORDER_DETAIL_PENDING } from "../constants/createOrderDetail";

const initialState = {
  createOrderDetail: {},
  pending: false,
  error: null
}

export default function createOrderDetailReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_ORDER_DETAIL_PENDING:
      return {
        ...state,
        pending: true
      };
    case CREATE_ORDER_DETAIL:
      return {
        ...state,
        createOrderDetail: action.payload
      }
    case CREATE_ORDER_DETAIL_ERROR:
      return {
        ...state,
        error: action.error,
        pending: false
      };
    default:
      return state;
  }
}
