import {
    PRODUCT_CHANGE,
    QUANTITY_CHANGE
} from "../constants/inputOrderDetailChange";

const initialState = {
    product: "none",
    quantity: 0,
}

export default function inputOrderDetailChangeReducer(state = initialState, action) {
    switch (action.type) {
        case PRODUCT_CHANGE:
            return {
                ...state,
                product: action.payload
            }
        case QUANTITY_CHANGE:
            return {
                ...state,
                quantity: action.payload
            }
        default:
            return state;
    }
}

