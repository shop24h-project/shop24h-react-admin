import { DELETE_ORDER_ERROR, DELETE_ORDER_SUCCESS } from "../constants/deleteOrder";

const initialState = {
  currentOrder: {},
  error: null,
}

export default function deleteOrderReducer(state = initialState, action) {
  switch (action.type) {
    case DELETE_ORDER_SUCCESS:
      return {
        ...state,
        currentOrder: action.payload
      }
    case DELETE_ORDER_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
