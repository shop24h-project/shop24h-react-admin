import { GET_ALL_ORDER_DETAIL} from "../constants/getOrderDetail";

const limit = 6;

const initialState = {
    currentPage: 1,
    orderDetails: [],
    countAll: 0,
    noPage: 0,
    orderInfo: "",
}

export default function getOrderDetailReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_ORDER_DETAIL:
            return {
                ...state,
                countAll : action.data.length,
                orderDetails: action.data,
                noPage: Math.ceil(action.data.length / limit),
            }
        default:
            return state;
    }
}

