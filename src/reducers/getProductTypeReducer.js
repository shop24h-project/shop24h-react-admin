import { GET_PRODUCT_TYPE_LIMIT, GET_ALL_PRODUCT_TYPE, GET_PRODUCT_TYPE_BY_ID } from "../constants/getProductType";


const initialState = {
    productTypes: [],
    currentProductType: {}
}

export default function getProductTypeReducer(state = initialState, action) {
    switch (action.type) {
        case GET_PRODUCT_TYPE_LIMIT:
            return {
                ...state,
                productTypes: action.data,
            }
        case GET_ALL_PRODUCT_TYPE:
            return {
                ...state,
                productTypes: action.data,
            }
            case GET_PRODUCT_TYPE_BY_ID:
                return {
                    ...state,
                    currentProductType: action.data,
                }
        default:
            return state;
    }
}

