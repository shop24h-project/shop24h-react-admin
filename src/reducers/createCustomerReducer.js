import { CREATE_CUSTOMER, CREATE_CUSTOMER_ERROR, CREATE_CUSTOMER_PENDING } from "../constants/createCustomer";

const initialState = {
  customer: {},
  pending: false,
  error: null
}

export default function createCustomerReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_CUSTOMER_ERROR:
      return {
        ...state,
        pending: true
      };
    case CREATE_CUSTOMER:
      return {
        ...state,
        customer: action.payload
      }
    case CREATE_CUSTOMER_PENDING:
      return {
        ...state,
        error: action.error,
        pending: false
      };
    default:
      return state;
  }
}

