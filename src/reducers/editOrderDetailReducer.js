import { EDIT_ORDER_DETAIL_ERROR, EDIT_ORDER_DETAIL_SUCCESS } from "../constants/editOrderDetail";

const initialState = {
  currentState: {},
  error: null,
}

export default function editOrderDetailReducer(state = initialState, action) {
  switch (action.type) {
    case EDIT_ORDER_DETAIL_SUCCESS:
      return {
        ...state,
        currentState: action.payload
      }
    case EDIT_ORDER_DETAIL_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}
