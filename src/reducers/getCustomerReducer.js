import { GET_ALL_CUSTOMER, GET_CUSTOMER_BY_KEY,CHANGE_PAGE_PAGINATION} from "../constants/getCustomer";

const limit = 6;

const initialState = {
    currentPage: 1,
    customers: [],
    countAll: 0,
    noPage: 0,
    customerInfo: "",
}

export default function getProductReducer(state = initialState, action) {
    switch (action.type) {
        case GET_ALL_CUSTOMER:
            return {
                ...state,
                countAll : action.data.length,
                customers: action.data.slice((state.currentPage - 1) * limit, state.currentPage * limit),
                noPage: Math.ceil(action.data.length / limit),
            }
            case GET_CUSTOMER_BY_KEY:
                return {
                    ...state,
                    customers: action.data,
                }
        case CHANGE_PAGE_PAGINATION:
            return {
                ...state,
                currentPage: action.payload
            }
        default:
            return state;
    }
}

