import { EDIT_PRODUCT_TYPE_ERROR, EDIT_PRODUCT_TYPE_SUCCESS } from "../constants/editProductType";

const initialState = {
  currentState: {},
  error: null,
}

export default function editProductTypeReducer(state = initialState, action) {
  switch (action.type) {
    case EDIT_PRODUCT_TYPE_SUCCESS:
      return {
        ...state,
        currentState: action.payload
      }
    case EDIT_PRODUCT_TYPE_ERROR:
      return {
        ...state,
        error: action.payload
      }
    default:
      return state;
  }
}

