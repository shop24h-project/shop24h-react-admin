import {
    FULL_NAME_CHANGE,
    PHONE_CHANGE,
    EMAIL_CHANGE,
    ADDRESS_CHANGE,
    CITY_CHANGE,
    COUNTRY_CHANGE,
} from "../constants/inputCustomerChange";

const initialState = {
    fullName: "",
    phone: "",
    email: "",
    address: "",
    city: "",
    country: "vn",
}

export default function inputCustomerChangeReducer(state = initialState, action) {
    switch (action.type) {
        case FULL_NAME_CHANGE:
            return {
                ...state,
                fullName: action.payload
            }
        case PHONE_CHANGE:
            return {
                ...state,
                phone: action.payload
            }
        case EMAIL_CHANGE:
            return {
                ...state,
                email: action.payload
            }
        case ADDRESS_CHANGE:
            return {
                ...state,
                address: action.payload
            }
        case CITY_CHANGE:
            return {
                ...state,
                city: action.payload
            }
        case COUNTRY_CHANGE:
            return {
                ...state,
                country: action.payload
            }
        default:
            return state;
    }
}

