import {
    GET_ALL_CUSTOMER,
    GET_CUSTOMER_BY_KEY,
    CHANGE_PAGE_PAGINATION
    } from "../constants/getCustomer";
    export function changePagePagination(value) {
        return {
            type: CHANGE_PAGE_PAGINATION,
            payload: value
        };
      }
    export const getAllCustomter= (fullName) => async dispatch => {
        var url =  `${process.env.REACT_APP_BACKEND_DOMAIN}/customers`;
        if(fullName){
            url += `?fullName=${fullName}`;
        }
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
      try {
          const response = await fetch(
            url, requestOptions
            )
            const data = await response.json();
            return dispatch({
                type: GET_ALL_CUSTOMER,
                data: data.Customer
            });
      } catch (err) {
          console.log(err);
      }
    };
    export const getCustomerByKey= (key) => async dispatch => {
        var url =  `${process.env.REACT_APP_BACKEND_DOMAIN}/customer-by`;
        if(key){
            url += `?key=${key}`;
        }
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
      try {
          const response = await fetch(
            url, requestOptions
            )
            const data = await response.json();
            return dispatch({
                type: GET_CUSTOMER_BY_KEY,
                data: data.customer
            });
      } catch (err) {
          console.log(err);
      }
    };