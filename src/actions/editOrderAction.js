import { EDIT_ORDER_SUCCESS, EDIT_ORDER_ERROR } from "../constants/editOrder";

export const editOrderAction = (orderId, state) => async dispatch => {

  var requestOptions = {
    method: 'PUT',
    body: JSON.stringify(state),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/orders/${orderId}`, requestOptions
    );

    const data = await response.json();

    await dispatch({
      type: EDIT_ORDER_SUCCESS,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: EDIT_ORDER_ERROR,
      error: err
    });
  }
};
