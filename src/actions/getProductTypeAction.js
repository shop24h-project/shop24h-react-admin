import {
    GET_PRODUCT_TYPE_LIMIT,
    GET_ALL_PRODUCT_TYPE,
    GET_PRODUCT_TYPE_BY_ID
    } from "../constants/getProductType";

    export const getProductTypeLimit= () => async dispatch => {
  
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
      try {
          const response = await fetch(
            `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types?limit=3`, requestOptions
            )
            const data = await response.json()
            return dispatch({
                type: GET_PRODUCT_TYPE_LIMIT,
                data: data.productType
            });
      } catch (err) {
          console.log(err);
      }
    };
    
    export const getAllProductType = (name) => async dispatch => {
        var url = `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types`;
        if(name){
            url += `?name=${name}` 
        }
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        }
        try {
            const response = await fetch(
                url, requestOptions
              )
              const data = await response.json()
              return dispatch({
                  type: GET_ALL_PRODUCT_TYPE,
                  data: data.productType
              });
        } catch (err) {
            console.log(err);
        }
      };
      export const getProductTypeById = (id) => async dispatch => {
        console.log("ab", id);
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
        }
      
        try {
            const response = await fetch(
              `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types/${id}`, requestOptions
              )
            const data = await response.json();
            return dispatch({
                type: GET_PRODUCT_TYPE_BY_ID,
                data: data.productType
            });
        } catch (err) {
            // console.log(err)
        }
      }
      
