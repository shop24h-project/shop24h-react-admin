import { EDIT_PRODUCT_SUCCESS, EDIT_PRODUCT_ERROR } from "../constants/editProduct";

export const editProductAction = (productId, state) => async dispatch => {

  var requestOptions = {
    method: 'PUT',
    body: JSON.stringify(state),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/products/${productId}`, requestOptions
    );

    const data = await response.json();

    await dispatch({
      type: EDIT_PRODUCT_SUCCESS,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: EDIT_PRODUCT_ERROR,
      error: err
    });
  }
};
