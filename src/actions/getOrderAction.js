import {
    GET_ALL_ORDER,
    CHANGE_PAGE_PAGINATION
    } from "../constants/getOrder";
    export function changePagePagination(value) {
        return {
            type: CHANGE_PAGE_PAGINATION,
            payload: value
        };
      }
    export const getAllOrder= () => async dispatch => {
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
      try {
          const response = await fetch(
            `${process.env.REACT_APP_BACKEND_DOMAIN}/orders`, requestOptions
            )
            const data = await response.json();
            return dispatch({
                type: GET_ALL_ORDER,
                data: data.data
            });
      } catch (err) {
          console.log(err);
      }
    };
    