import {
    PRODUCT_CHANGE,
    QUANTITY_CHANGE,
} from "../constants/inputOrderDetailChange";

export function productChange(value) {
    return {
         type: PRODUCT_CHANGE,
         payload: value
    }
}

export function quantityChange(value) {
    return {
         type: QUANTITY_CHANGE,
         payload: value
    }
}

