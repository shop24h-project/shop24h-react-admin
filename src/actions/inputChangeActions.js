import {
    NAME_CHANGE,
    IMAGE_URL_CHANGE,
    DESCRIPTION_CHANGE,
} from "../constants/inputChange";

export function nameChange(value) {
    return {
         type: NAME_CHANGE,
         payload: value
    }
}

export function imageUrlChange(value) {
    return {
         type: IMAGE_URL_CHANGE,
         payload: value
    }
}

export function descriptionChange(value) {
    return {
         type: DESCRIPTION_CHANGE,
         payload: value
    }
}




  
