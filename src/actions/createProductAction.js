import { CREATE_PRODUCT, CREATE_PRODUCT_ERROR, CREATE_PRODUCT_PENDING } from "../constants/createProduct";

export const createProductAction = (formInput) => async dispatch => {
  var requestOptions = {
    method: 'POST',
    body: JSON.stringify(formInput),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  await dispatch({
    type: CREATE_PRODUCT_PENDING
  });

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/products`, requestOptions
    );

    const data = await response.json();

    return dispatch({
      type: CREATE_PRODUCT,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: CREATE_PRODUCT_ERROR,
      error: err
    });
  }
};

