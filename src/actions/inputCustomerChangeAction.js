import {
    FULL_NAME_CHANGE,
    PHONE_CHANGE,
    EMAIL_CHANGE,
    ADDRESS_CHANGE,
    CITY_CHANGE,
    COUNTRY_CHANGE,
} from "../constants/inputCustomerChange";

export function fullNameChange(value) {
    return {
         type: FULL_NAME_CHANGE,
         payload: value
    }
}

export function phoneChange(value) {
    return {
         type: PHONE_CHANGE,
         payload: value
    }
}

export function emailChange(value) {
    return {
         type: EMAIL_CHANGE,
         payload: value
    }
}
export function addressChange(value) {
    return {
         type: ADDRESS_CHANGE,
         payload: value
    }
}
export function cityChange(value) {
    return {
         type: CITY_CHANGE,
         payload: value
    }
}
export function countryChange(value) {
    return {
         type: COUNTRY_CHANGE,
         payload: value
    }
}




