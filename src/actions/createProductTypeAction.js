import { CREATE_PRODUCT_TYPE, CREATE_PRODUCT_TYPE_ERROR, CREATE_PRODUCT_TYPE_PENDING } from "../constants/createProductType";

export const createProductTypeAction = (formInput) => async dispatch => {
  var requestOptions = {
    method: 'POST',
    body: JSON.stringify(formInput),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  await dispatch({
    type: CREATE_PRODUCT_TYPE_PENDING
  });

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types`, requestOptions
    );

    const data = await response.json();

    return dispatch({
      type: CREATE_PRODUCT_TYPE,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: CREATE_PRODUCT_TYPE_ERROR,
      error: err
    });
  }
};

