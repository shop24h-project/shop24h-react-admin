import {
    NAME_CHANGE,
    DESCRIPTION_CHANGE,
    TYPE_CHANGE,
    IMAGE_URL_CHANGE,
    BUY_PRICE_CHANGE,
    PROMORTION_PRICE_CHANGE,
    AMOUNT_CHANGE,
    IS_POPULAR_CHANGE,
    COLOR_CHANGE,
    SUB_IMAGE_CHANGE
} from "../constants/inputProductChange";

export function nameChange(value) {
    return {
         type: NAME_CHANGE,
         payload: value
    }
}

export function descriptionChange(value) {
    return {
         type: DESCRIPTION_CHANGE,
         payload: value
    }
}

export function typeChange(value) {
    return {
         type: TYPE_CHANGE,
         payload: value
    }
}
export function imageUrlChange(value) {
    return {
         type: IMAGE_URL_CHANGE,
         payload: value
    }
}
export function buyPriceChange(value) {
    return {
         type: BUY_PRICE_CHANGE,
         payload: value
    }
}
export function promotionPriceChange(value) {
    return {
         type: PROMORTION_PRICE_CHANGE,
         payload: value
    }
}export function amountChange(value) {
    return {
         type: AMOUNT_CHANGE,
         payload: value
    }
}
export function isPopularChange(value) {
    return {
         type: IS_POPULAR_CHANGE,
         payload: value
    }
}
export function colorChange(value) {
    return {
         type: COLOR_CHANGE,
         payload: value
    }
}
export function subImageChange(value) {
    return {
         type: SUB_IMAGE_CHANGE,
         payload: value
    }
}






