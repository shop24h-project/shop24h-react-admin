import {
    GET_ALL_ORDER_DETAIL,
    // CHANGE_PAGE_PAGINATION
    } from "../constants/getOrderDetail";
    // export function changePagePagination(value) {
    //     return {
    //         type: CHANGE_PAGE_PAGINATION,
    //         payload: value
    //     };
    //   }
    export const getAllOrderDetail= (orderId) => async dispatch => {
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
      console.log("api")
      try {
          const response = await fetch(
            `${process.env.REACT_APP_BACKEND_DOMAIN}/orders/${orderId}/order-details`, requestOptions
            )
            const data = await response.json();
            return dispatch({
                type: GET_ALL_ORDER_DETAIL,
                data: data.data
            });
      } catch (err) {
          console.log(err);
      }
    };
    