import {
    COST_CHANGE,
    NOTE_CHANGE,
} from "../constants/inputOrderChange";

export function costChange(value) {
    return {
         type: COST_CHANGE,
         payload: value
    }
}

export function noteChange(value) {
    return {
         type: NOTE_CHANGE,
         payload: value
    }
}







