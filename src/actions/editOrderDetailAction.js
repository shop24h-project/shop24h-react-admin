import { EDIT_ORDER_DETAIL_SUCCESS, EDIT_ORDER_DETAIL_ERROR } from "../constants/editOrderDetail";

export const editOrderDetailAction = (orderDetailId, state) => async dispatch => {

  var requestOptions = {
    method: 'PUT',
    body: JSON.stringify(state),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/order-details/${orderDetailId}`, requestOptions
    );

    const data = await response.json();

    await dispatch({
      type: EDIT_ORDER_DETAIL_SUCCESS,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: EDIT_ORDER_DETAIL_ERROR,
      error: err
    });
  }
};
