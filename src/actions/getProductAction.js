import {
    GET_PRODUCT_BY_ID,
    GET_ALL_PRODUCT,
    GET_POPULAR_PRODUCT,
    CHANGE_PAGE_PAGINATION
    } from "../constants/getProduct";
    export function changePagePagination(value) {
      return {
          type: CHANGE_PAGE_PAGINATION,
          payload: value
      };
    }
    // export const filterProductsByPrice = (priceMin, priceMax) => async dispatch => {
    //   var requestOptions = {
    //       method: 'GET',
    //       redirect: 'follow'
    //   }
    
    //   try {
    //       const response = await fetch(
    //         `${process.env.REACT_APP_BACKEND_DOMAIN}/products?priceMin=${priceMin}&priceMax=${priceMax}`, requestOptions
    //         )
    //       const data = await response.json();
    //       return dispatch({
    //           type: GET_PRODUCT_BY_ID,
    //           data: data.product,
    //           priceMin: priceMin,
    //           priceMax: priceMax
    //       });
    //   } catch (err) {
    //       // console.log(err)
    //   }
    // }
    
    
    export const getAllProduct= (name) => async dispatch => {
        var url = `${process.env.REACT_APP_BACKEND_DOMAIN}/products`;
        if(name){
            url += `?name=${name}` 
        }
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
      try {
          const response = await fetch(
            url, requestOptions
            )
            const data = await response.json();
            return dispatch({
                type: GET_ALL_PRODUCT,
                data: data.product
            });
      } catch (err) {
          console.log(err);
      }
    };
    export const getPopularProduct= () => async dispatch => {
  
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
      try {
          const response = await fetch(
            `${process.env.REACT_APP_BACKEND_DOMAIN}/products?isPopular=true&limit=4`, requestOptions
            )
            const data = await response.json();
            console.log(1111111111)
            return dispatch({
                type: GET_POPULAR_PRODUCT,
                data: data.product
            });
      } catch (err) {
          console.log(err);
      }
    };
    
    export const getProductById = (id) => async dispatch => {
      console.log("ab", id);
      var requestOptions = {
          method: 'GET',
          redirect: 'follow'
      }
    
      try {
          const response = await fetch(
            `${process.env.REACT_APP_BACKEND_DOMAIN}/products/${id}`, requestOptions
            )
          const data = await response.json();
          return dispatch({
              type: GET_PRODUCT_BY_ID,
              payload: data.product
          });
      } catch (err) {
          // console.log(err)
      }
    }
    
  