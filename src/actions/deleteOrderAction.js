import { DELETE_ORDER_SUCCESS, DELETE_ORDER_ERROR } from "../constants/deleteOrder";

export const deleteOrderAction = (orderId, customerId) => async dispatch => {

  var requestOptions = {
    method: 'DELETE',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
       `${process.env.REACT_APP_BACKEND_DOMAIN}/customers/${customerId}/orders/${orderId}`, requestOptions
    );

    return dispatch({
      type: DELETE_ORDER_SUCCESS,
      payload: `Delete Customer success`
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: DELETE_ORDER_ERROR,
      error: err
    });
  }
};
