import { CREATE_CUSTOMER, CREATE_CUSTOMER_ERROR, CREATE_CUSTOMER_PENDING } from "../constants/createCustomer";

export const createCustomerAction = (formInput) => async dispatch => {
  var requestOptions = {
    method: 'POST',
    body: JSON.stringify(formInput),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  await dispatch({
    type: CREATE_CUSTOMER_PENDING
  });

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/customers`, requestOptions
    );

    const data = await response.json();
    
    return dispatch({
      type: CREATE_CUSTOMER,
      payload: data
    });
  } catch (err) {
    await dispatch({
      type: CREATE_CUSTOMER_ERROR,
      error: err
    });
  }
};

