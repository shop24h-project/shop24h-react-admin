import { EDIT_PRODUCT_TYPE_SUCCESS, EDIT_PRODUCT_TYPE_ERROR } from "../constants/editProductType";

export const editProductTypeAction = (productTypeId, state) => async dispatch => {

  var requestOptions = {
    method: 'PUT',
    body: JSON.stringify(state),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types/${productTypeId}`, requestOptions
    );

    const data = await response.json();

    await dispatch({
      type: EDIT_PRODUCT_TYPE_SUCCESS,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: EDIT_PRODUCT_TYPE_ERROR,
      error: err
    });
  }
};
