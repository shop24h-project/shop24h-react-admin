import { DELETE_ORDER_DETAIL_SUCCESS, DELETE_ORDER_DETAIL_ERROR } from "../constants/deleteOrderDetail";

export const deleteOrderDetailAction = (orderId, orderDetailId) => async dispatch => {

  var requestOptions = {
    method: 'DELETE',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
       `${process.env.REACT_APP_BACKEND_DOMAIN}/orders/${orderId}/order-details/${orderDetailId}`, requestOptions
    );

    return dispatch({
      type: DELETE_ORDER_DETAIL_SUCCESS,
      payload: `Delete Order Detail success`
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: DELETE_ORDER_DETAIL_ERROR,
      error: err
    });
  }
};
