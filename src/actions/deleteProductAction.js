import { DELETE_PRODUCT_SUCCESS, DELETE_PRODUCT_ERROR } from "../constants/deleteProduct";

export const deleteProductAction = (productId) => async dispatch => {

  var requestOptions = {
    method: 'DELETE',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
       `${process.env.REACT_APP_BACKEND_DOMAIN}/products/${productId}`, requestOptions
    );

    return dispatch({
      type: DELETE_PRODUCT_SUCCESS,
      payload: `Delete Product success`
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: DELETE_PRODUCT_ERROR,
      error: err
    });
  }
};

