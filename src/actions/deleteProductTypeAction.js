import { DELETE_PRODUCT_TYPE_SUCCESS, DELETE_PRODUCT_TYPE_ERROR } from "../constants/deleteProductType";

export const deleteProductTypeAction = (productTypeId) => async dispatch => {

  var requestOptions = {
    method: 'DELETE',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
       `${process.env.REACT_APP_BACKEND_DOMAIN}/product-types/${productTypeId}`, requestOptions
    );

    return dispatch({
      type: DELETE_PRODUCT_TYPE_SUCCESS,
      payload: `Delete Product Type success`
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: DELETE_PRODUCT_TYPE_ERROR,
      error: err
    });
  }
};

