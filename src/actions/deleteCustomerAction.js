import { DELETE_CUSTOMER_SUCCESS, DELETE_CUSTOMER_ERROR } from "../constants/deleteCustomer";

export const deleteCustomerAction = (customerId) => async dispatch => {

  var requestOptions = {
    method: 'DELETE',
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
       `${process.env.REACT_APP_BACKEND_DOMAIN}/customers/${customerId}`, requestOptions
    );

    return dispatch({
      type: DELETE_CUSTOMER_SUCCESS,
      payload: `Delete Customer success`
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: DELETE_CUSTOMER_ERROR,
      error: err
    });
  }
};

