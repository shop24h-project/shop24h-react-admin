import { EDIT_CUSTOMER_SUCCESS, EDIT_CUSTOMER_ERROR } from "../constants/editCustomer";

export const editCustomerAction = (customerId, state) => async dispatch => {

  var requestOptions = {
    method: 'PUT',
    body: JSON.stringify(state),
    headers: {
      'Content-type': 'application/json; charset=UTF-8',
    }
  }

  try {
    const response = await fetch(
        `${process.env.REACT_APP_BACKEND_DOMAIN}/customers/${customerId}`, requestOptions
    );

    const data = await response.json();

    await dispatch({
      type: EDIT_CUSTOMER_SUCCESS,
      payload: data
    });
  } catch (err) {
    // alert(err);
    await dispatch({
      type: EDIT_CUSTOMER_ERROR,
      error: err
    });
  }
};
