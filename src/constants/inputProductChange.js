export const NAME_CHANGE = 'NAME_CHANGE';
export const DESCRIPTION_CHANGE = 'DESCRIPTION_CHANGE';
export const TYPE_CHANGE = 'TYPE_CHANGE';
export const IMAGE_URL_CHANGE = 'IMAGE_URL_CHANGE';
export const BUY_PRICE_CHANGE = 'BUY_PRICE_CHANGE';
export const PROMORTION_PRICE_CHANGE = 'PROMORTION_PRICE_CHANGE';
export const AMOUNT_CHANGE = 'AMOUNT_CHANGE';
export const IS_POPULAR_CHANGE = 'IS_POPULAR_CHANGE';
export const COLOR_CHANGE = 'COLOR_CHANGE';
export const SUB_IMAGE_CHANGE = 'SUB_IMAGE_CHANGE';