
import { Container, Button, Grid, TableContainer, Paper, Table, TableHead, TableCell, TableRow, TableBody, Pagination } from "@mui/material";
import { getAllCustomter, changePagePagination } from "../../actions/getCustomerAction";
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import CreateCustomerModal from './CreateCustomerModal';
import DeleteCustomerModal from './DeleteCustomerModal';
import EditCustomerModal from "./EditCustomerModal";
// import { deleteCustomerAction } from "../../actions/deleteCustomerAction";
const CustomerList = () => {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4
  };
  const dispatch = useDispatch();
  const { countAll, currentPage, customers, noPage } = useSelector((reduxData) => reduxData.getCustomerReducer);

  const [openModalAdd, setOpenModalAdd] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [varRefeshPage, setVarRefeshPage] = useState(0);
  const [customerId, setCustomerId]= useState(0);
  const [currentCustomer, setCurrentCustomer]= useState(0);
  const [filterKey, setFilterKey] = useState('');
  const handleClose = () => setOpenModalAdd(false);
  const handleCloseEdit = () => setOpenModalEdit(false);
  const handleCloseDelete = () => setOpenModalDelete(false);
  const onBtnAddCustomerClick = () => {
    setOpenModalAdd(true)
  }
  useEffect(() => {
    dispatch(getAllCustomter(filterKey));
  }, [currentPage, varRefeshPage, filterKey]);
  const onChangePagination = (event, value) => {
    dispatch(changePagePagination(value));
  }
  const onBtnDeleteClick = (paramCustomerId) => {
    setCustomerId(paramCustomerId);
    setOpenModalDelete(true)
  }
  const onBtnEditClick = (paramCustomer) => {
    setCurrentCustomer(paramCustomer);
    setOpenModalEdit(true);
}
const handleFilter = (event) => {
  setFilterKey(event.target.value)
}

  return (
    <div>
      <Container>
      <Button onClick={onBtnAddCustomerClick} openModalAdd={openModalAdd} handleClose={handleClose} className="mb-2" value="add-user" style={{ borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px" }} variant="contained">Add Customer +</Button>
      <input
              type="text"
              className="form-input-filter"
              style={{ padding: "5px 10px", float: "right"}}
              onChange={handleFilter}
              placeholder="Search.."
            />
        <Grid item style={{ width: "100%" }} >
          <TableContainer component={Paper} >
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Full Name </TableCell>
                  <TableCell align="center">Phone</TableCell>
                  <TableCell align="center">Email</TableCell>
                  <TableCell align="center">Address</TableCell>
                  <TableCell align="center">City</TableCell>
                  <TableCell align="center">Country</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {customers.map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align="center" component="th" scope="row">
                      {row.fullName}
                    </TableCell>
                    <TableCell align="center">{row.phone}</TableCell>
                    <TableCell align="center">{row.email}</TableCell>
                    <TableCell align="center">{row.address}</TableCell>
                    <TableCell align="center">{row.city}</TableCell>
                    <TableCell align="center">{row.country}</TableCell>
                    <TableCell align="center">
                      <Button onClick={() => onBtnEditClick((row))} style={{ borderRadius: 25, backgroundColor: "#2196f3", padding: "10px 20px", fontSize: "10px" }} variant="contained">Sửa</Button>
                      <Button onClick={() => { onBtnDeleteClick((row._id)) }} style={{ borderRadius: 25, backgroundColor: "#f50057", marginLeft: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained">Xóa</Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        {/* <!-- Pagination --> */}
        <Grid item xs={12} md={12} sm={12} lg={12} style={{marginTop: "10px"}}>
          <Grid container justifyContent={{ xs: "center", sm: "flex-end" }} >
            <Pagination
              count={noPage}
              defaultPage={1}
              onChange={onChangePagination}
              component="div"
            />
          </Grid>
        </Grid>
        <CreateCustomerModal  style={style} openModalAdd={openModalAdd} setOpenModalAdd={setOpenModalAdd} handleClose={handleClose} varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} />
        <DeleteCustomerModal customerId={customerId} openModalDelete={openModalDelete} style={style} handleCloseDelete={handleCloseDelete}/>
        <EditCustomerModal currentCustomer={currentCustomer} setOpenModalEdit={setOpenModalEdit} openModalEdit={openModalEdit} style={style} handleCloseEdit={handleCloseEdit}/>
      </Container>
    </div>
  )
}

export default CustomerList
