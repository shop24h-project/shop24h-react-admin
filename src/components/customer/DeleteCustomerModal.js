import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteCustomerAction } from '../../actions/deleteCustomerAction';
import { getAllCustomter } from "../../actions/getCustomerAction";
function DeleteProductTypeModal ({style, openModalDelete, handleCloseDelete,customerId}) {
    const dispatch = useDispatch();
    const [openAlert, setOpenAlert] = useState(false)
    const [statusModalDelete, setStatusModalDelete] = useState("error")
    const [noidungAlert, setNoidungAlert] = useState("");

    // const { currentProductType } = useSelector((reduxData) => reduxData.getProductTypeReducer);
    const { error } = useSelector((reduxData) => reduxData.deleteCustomerReducer);
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleCloseDelete()
    }
    const onBtnConfirmDeleteClick = () => {
        dispatch(deleteCustomerAction(customerId))
    if (error) {
      setOpenAlert(true)
      handleCloseDelete()
      setStatusModalDelete('error')
      setNoidungAlert('Delete order fail')
      setVarRefeshPage(varRefeshPage + 1);
    } else {
      setOpenAlert(true)
      setStatusModalDelete('success')
      handleCloseDelete()
      setNoidungAlert('Delete order success')
      setVarRefeshPage(varRefeshPage + 1);
    }

    }

    
  useEffect(() => {
    dispatch(getAllCustomter());
  }, [varRefeshPage]);
    return (
        <>
            <Modal
            open={openModalDelete}
            onClose={handleCloseDelete}
            aria-labelledby="modal-delete"
            aria-describedby="modal-delete-user"
            >
            <Box sx={style}>
                <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                    <strong>Delete User!</strong>
                </Typography>
                    <Row className="mt-2">
                        <Col sm="12">
                            <p>Bạn có thật sự muốn xóa User <strong>{customerId}</strong> chứ?</p>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onBtnConfirmDeleteClick} className="bg-danger w-100 text-white">Xác nhận</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
            </Box>
        </Modal>
        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={statusModalDelete} sx={{ width: '100%' }}>
            {noidungAlert}
            </Alert>
      </Snackbar>
        </>
    )
}

export default DeleteProductTypeModal

