import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createCustomerAction } from '../../actions/createCustomerAction';
import { getAllCustomter } from "../../actions/getCustomerAction";
import {
    fullNameChange,
    phoneChange,
    emailChange,
    addressChange,
    cityChange,
    countryChange,
} from "../../actions/inputCustomerChangeAction";



function CreateCustomerModal({ style, openModalAdd, setOpenModalAdd, handleClose, varRefeshPage, setVarRefeshPage}) {
    const dispatch = useDispatch();
    const { fullName, phone, email, address, city, country } = useSelector((reduxData) => reduxData.inputCustomerChangeReducer);
    const { error } = useSelector((reduxData) => reduxData.createCustomerReducer);
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")

    const onFullNameChange = (event) => {
        dispatch(fullNameChange(event.target.value));
    }
    const onPhoneChange = (event) => {
        dispatch(phoneChange(event.target.value));
    }
    const onEmailChange = (event) => {
        dispatch(emailChange(event.target.value));
    }
    const onAddressChange = (event) => {
        dispatch(addressChange(event.target.value));
    }
    const onCityChange = (event) => {
        dispatch(cityChange(event.target.value));
    }
    const onCountryChange = (event) => {
        dispatch(countryChange(event.target.value));
    }
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleClose();
    }

    const onBtnCreateDataClick = () => {
        var newData = {
            fullName: fullName,
            phone: phone,
            email: email,
            address: address,
            city: city,
            country: country,
        }
        var validateData = validateNewData(newData);
        if (validateData) {
            dispatch(createCustomerAction(newData));
            console.log(newData);
            if (!error) {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("Add data success!")
                setVarRefeshPage(varRefeshPage + 1);
                handleClose();
                // clearInput();
            } else {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Add data failed!")
                console.log(error);
                setVarRefeshPage(varRefeshPage + 1);
            }
        }
    }
    const validateNewData = (paramNewData) => {
        if (paramNewData.fullName === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Full Name is required!");
            return false;
        }
        if (paramNewData.phone === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Phone is required!");
            return false;
        }
        var vRegexStr = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        if (!vRegexStr.test(paramNewData.email)) {
          setOpenAlert(true);
          setStatusModal("error")
          setNoidungAlertValid("Email is invalid!");
          return false;
        }

        if (paramNewData.address === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Address is required!");
            return false;
        } 
        if (paramNewData.city === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("City is required!");
            return false;
        } 
        if (paramNewData.country === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Country is required!");
            return false;
        } 
         else {
            return true;
        }

    }
    return (
        <>
            <Modal
                open={openModalAdd}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Create Customer!</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">

                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Full Name</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                    onChange={onFullNameChange}
                                    value={fullName}
                                    // style = {{ background: '#f0f0f0'}}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Phone</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        onChange={onPhoneChange}
                                        value={phone}
                                    // style = {{ background: '#f0f0f0'}}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Email</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                    onChange={onEmailChange}
                                    value={email}
                                    // style = {{ background: '#f0f0f0'}}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Address</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                    onChange={onAddressChange}
                                    value={address}
                                    // style = {{ background: '#f0f0f0'}}
                                    />
                                </Col>
                            </Row>
                          

                        </Col>
                        <Col sm="6">
                        <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Country</label>
                                </Col>
                                <Col sm="7">
                                <Select
                                                MenuProps={{
                                                    disableScrollLock: true,
                                                }}
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={country}
                                                onChange={onCountryChange}
                                                style={{ width: "100%" }}
                                                disableScrollLock={true}
                                            >  
                                                 <MenuItem value="vn">Viet Nam</MenuItem>
                                                <MenuItem value="usa">United States</MenuItem>
                                                <MenuItem value="uk">United Kingdom</MenuItem>
                                                <MenuItem value="ger">Germany</MenuItem>
                                                <MenuItem value="fra">France</MenuItem>
                                                <MenuItem value="ind">India</MenuItem>
                                                <MenuItem value="aus">Australia</MenuItem>
                                                <MenuItem value="bra">Brazil</MenuItem>
                                                <MenuItem value="cana">Canada</MenuItem>
                                            </Select>
                                </Col>
                            </Row>

                          
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>City</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        value={city}
                                    onChange={onCityChange}

                                    // style = {{ background: '#f0f0f0'}}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="7">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={onBtnCreateDataClick} >Create Customer</Button>
                                </Col>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={onBtnCancelClick} >Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}
export default CreateCustomerModal
