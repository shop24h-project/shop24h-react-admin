import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCustomerByKey } from "../../actions/getCustomerAction";
import OrderAddModal from "./OrderAddModal";
function OrderUserModal({ style, openModalUser, handleCloseUser }) {
    const dispatch = useDispatch();
    const { customers } = useSelector((reduxData) => reduxData.getCustomerReducer);
    const [openAlert, setOpenAlert] = useState(false);
    const [noidungAlertValid, setNoidungAlertValid] = useState("");
    const [statusModal, setStatusModal] = useState("error");
    const [openModalAdd, setOpenModalAdd] = useState(false);
    const [key, setKey] = useState("");
    const handleCloseAdd = () => {
        setOpenModalAdd(false);
    }
    const handleCloseAlert = () => {
        setOpenAlert(false);
    }
    const onKeyChange = (event) => {
        setKey(event.target.value);
    }
    const handleUser = () => {
        if(customers){
            handleCloseUser();
         setOpenModalAdd(true);
        }
        else{
            setOpenAlert(true)
            setStatusModal('error');
            setNoidungAlertValid('Info Customer is not correct');
        }
    }
    const handleCancel = () => {
        handleCloseUser();
    }

    // useEffect(() => {
    //     loadDataToInput();
    // }, [openModalEdit]);

    useEffect(() => {
        if(key !== ""){
            dispatch(getCustomerByKey(key));
        }
      },[key]);
    return (
        <>
               <Modal
                open={openModalUser}
                onClose={handleCloseUser}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Add Product!</strong>
                    </Typography>
                    <Row>
                        <Col sm="8">
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Enter Email or Phone Number</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                    onChange={onKeyChange}
                                    value={key}
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="7">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleUser} >Continue</Button>
                                </Col>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleCancel}>Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
            {
                customers ?
            <OrderAddModal customers={customers} setOpenModalAdd={setOpenModalAdd} openModalAdd={openModalAdd} style={style} handleCloseAdd={handleCloseAdd}/>
                :
                ""
            }
           
        </>
    )
}
export default OrderUserModal

