
import { Container, Button, Grid, TableContainer, Paper, Table, TableHead, TableCell, TableRow, TableBody, Pagination } from "@mui/material";
import { getAllOrder, changePagePagination } from "../../actions/getOrderAction";
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import OrderDetailModal from "./OrderDetailModal";
import OrderUserModal from "./OrderUserModal";
import OrderDeleteModal from "./OrderDeleteModal";
import OrderEditModal from "./OrderEditModal";
import { getAllProduct } from "src/actions/getProductAction";
const ProductList = () => {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4
  };
  const dispatch = useDispatch();
  const { countAll, currentPage, orders, noPage } = useSelector((reduxData) => reduxData.getOrderReducer);
  const [openModalDetail, setOpenModalDetail] = useState(false);
  const handleCloseDetail = () => setOpenModalDetail(false);
  const handleCloseEdit = () => setOpenModalEdit(false);
  const handleCloseDelete = () => setOpenModalDelete(false);
  const handleCloseUser = () => setOpenModalUser(false);
  const [varRefeshPage, setVarRefeshPage] = useState(0);
  const [openModalUser, setOpenModalUser] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [orderId, setOrderId] = useState("");
  const [customerId, setCustomerId] = useState("");
  const [currentOrder, setCurrentOrder] = useState("");
  useEffect(() => {
    dispatch(getAllOrder());
  }, [currentPage, varRefeshPage]);
  const onChangePagination = (event, value) => {
    dispatch(changePagePagination(value));
  }
const onBtnShowOrderDetail = (paramOrderId) => {
  setOrderId(paramOrderId);
  setOpenModalDetail(true);
}
const onBtnAddOrderClick = () => {
  setOpenModalUser(true);
}
const onBtnDeleteClick = (paramOrderId, paramCustomerId) => {
  setOrderId(paramOrderId);
  setCustomerId(paramCustomerId);
  setOpenModalDelete(true);
}
const onBtnEditClick = (paramCurrentOrder) => {
  setCurrentOrder(paramCurrentOrder);
  setOpenModalEdit(true);
}
  return (
    <div>
      <Container>
      <Button onClick={onBtnAddOrderClick} className="mb-2" value="add-user" style={{ borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px" }} variant="contained">Add Order +</Button>
        <Grid item style={{ width: "100%" }} >
          <TableContainer component={Paper} >
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Order Date </TableCell>
                  <TableCell align="center">Shipped Date</TableCell>
                  <TableCell align="center">Note</TableCell>
                  <TableCell align="center">Cost</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {orders.map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
  
                    <TableCell align="center">{row.orderDate?.slice(0, 10)}</TableCell>
                    <TableCell align="center">{row.shippedDate?.slice(0, 10)}</TableCell>
                    <TableCell align="center">{row.note}</TableCell>
                    <TableCell align="center">{row.cost}</TableCell>
                    <TableCell align="center">
                    <Button style={{ borderRadius: 25, backgroundColor: "#f50057", marginLeft: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained" onClick={() => { onBtnEditClick(row) }}>Edit Order</Button>
                      <Button style={{ borderRadius: 25, backgroundColor: "#2196f3", marginLeft: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained" onClick={() => { onBtnDeleteClick(row._id, row.customerId) }}>Delete Order</Button>
                      <Button onClick={() => { onBtnShowOrderDetail(row._id) }} style={{ borderRadius: 25, backgroundColor: "#f50057", marginLeft: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained">Order Detail</Button>
                    </TableCell>

                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        {/* <!-- Pagination --> */}
        <Grid item xs={12} md={12} sm={12} lg={12} style={{marginTop: "10px"}}>
          <Grid container justifyContent={{ xs: "center", sm: "flex-end" }} >
            <Pagination
              count={noPage}
              defaultPage={1}
              onChange={onChangePagination}
              component="div"
            />
          </Grid>
        </Grid>
        <OrderDetailModal setOpenModalDetail={setOpenModalDetail} orderId={orderId} openModalDetail={openModalDetail} style={style} handleCloseDetail={handleCloseDetail}/>
        <OrderUserModal varRefeshPage = {varRefeshPage} setVarRefeshPage = {setVarRefeshPage} setOpenModalUser={setOpenModalUser} openModalUser={openModalUser} style={style} handleCloseUser={handleCloseUser}/>
        <OrderEditModal  currentOrder = {currentOrder} setOpenModalEdit={setOpenModalEdit} openModalEdit={openModalEdit} style={style} handleCloseEdit={handleCloseEdit}/>
        <OrderDeleteModal openModalDelete={openModalDelete} customerId={customerId} orderId={orderId} style={style} handleCloseDelete={handleCloseDelete}/> 
    
      </Container>
    </div>
  )
}

export default ProductList
