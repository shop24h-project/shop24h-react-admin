import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
   costChange,
   noteChange
} from "../../actions/inputOrderChangeAction";

import { getAllProduct } from "src/actions/getProductAction";
import { createOrderOfCustomerActions } from '../../actions/createOrderAction';
import { getAllOrder } from "src/actions/getOrderAction";
function OrderAddModal({ style, openModalAdd, handleCloseAdd, customers }) {
    const dispatch = useDispatch();
    const { cost, note } = useSelector((reduxData) => reduxData.inputOrderChangeReducer);
    const { error } = useSelector((reduxData) => reduxData.createOrderReducer);
    const [openAlert, setOpenAlert] = useState(false);
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error");
    const colorArr = [
        { value: "red", name: "Red" },
        { value: "green", name: "Green" },
        { value: "purple", name: "Purple" },
        { value: "blue", name: "Blue" },
        { value: "white", name: "White" }
    ]

    const onBtnCreateDataClick = () => {
        let today = new Date();
        var newData = {
            orderDate: today,
            requiredDate: today,
            shippedDate: today,
            note: note,
            cost: cost,
            customerId: customers._id
        }
        var validateData = validateNewData(newData);
        if (validateData) {
            console.log(customers._id)
            dispatch(createOrderOfCustomerActions(customers._id, newData));
            console.log(newData);
            if (!error) {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("Add data success!")
                setVarRefeshPage(varRefeshPage + 1);
                handleCloseAdd();
                clearInput();
            } else {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Add data failed!")
                console.log(error);
            }
        }
    }
    const validateNewData = (paramNewData) => {
        if (paramNewData.cost === 0) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Cost is invalid!");
            return false;
        }
         else {
            return true;
        }

    }
    const handleCloseAlert = () => {
        setOpenAlert(false);
    }


    const onChangeCost = (event) => {
        dispatch(costChange(event.target.value))
    }

    const onChangeNote= (event) => {
        dispatch(noteChange(event.target.value))
    }
    const clearInput = () => {
        dispatch(noteChange(""));
        dispatch(costChange(0));
    }


    // useEffect(() => {
    //     loadDataToInput();
    // }, [openModalEdit]);
    useEffect(() => {
        dispatch(getAllOrder());
      }, [varRefeshPage]);
    // useEffect(() => {
    //     dispatch(getAllProduct());
    //   }, [varRefeshPage]);
    return (
        <>
               <Modal
                open={openModalAdd}
                onClose={handleCloseAdd}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Add new order</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Name</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                    fullWidth
                                    value={customers.fullName}
                                    disabled
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Phone </label>
                                </Col>
                                <Col sm="7">
                                <Input
                                    fullWidth
                                    value={customers.phone}
                                    disabled
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Email</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                   disabled
                                    value={customers.email}
                                    />
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="6">
                      
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Cost</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        type = "number"
                                    value={cost}
                                    onChange={onChangeCost}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col sm="5" >
                                    <label>Note </label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={note}
                                        onChange={onChangeNote}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="7">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white"  onClick={onBtnCreateDataClick}>Create Order</Button>
                                </Col> 
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" >Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>

        </>
    )
}
export default OrderAddModal

