import { Container, TableContainer, Alert, Paper, Box, Button, fabClasses, Grid, Pagination, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize, Table, TableHead, TableCell, TableRow, TableBody } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllOrderDetail } from "../../actions/getOrderDetailAction";
import OrderDetailAddModal from "./OrderDetailAddModal";
import OrderDetailEditModal from "./OrderDetailEditModal";
import OrderDetailDeleteModal from "./OrderDetailDeleteModal";
import { getAllProduct } from "../../actions/getProductAction";
import { element } from "prop-types";
function OrderDetailModal({ style, openModalDetail, handleCloseDetail, orderId }) {
    const dispatch = useDispatch();
    const [openAlert, setOpenAlert] = useState(false);
    const { orderDetails } =  useSelector((reduxData) => reduxData.getOrderDetailReducer);
    const { products } = useSelector((reduxData) => reduxData.getProductReducer);
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error");
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const [currentOrderDetail, setCurrentOrderDetail] = useState(0);
    const [openModalDetailAdd, setOpenModalDetailAdd] = useState(false);
    const [openModalDetailEdit, setOpenModalDetailEdit] = useState(false);
    const [openModalDetailDelete, setOpenModalDetailDelete] = useState(false);
    const handleCloseDetailAdd = () => setOpenModalDetailAdd(false);
    const handleCloseDetailEdit = () => setOpenModalDetailEdit(false);
    const handleCloseDetailDelete = () => setOpenModalDetailDelete(false);
    const filterItems = (arr, id) => {
            var result = arr.filter(item => item._id === id);
            return result[0].name;
        }
    const colorArr = [
        { value: "red", name: "Red" },
        { value: "green", name: "Green" },
        { value: "purple", name: "Purple" },
        { value: "blue", name: "Blue" },
        { value: "white", name: "White" }
    ]

    // const loadDataToInput = () => {
    //     dispatch(nameChange(currentProduct.name));
    //     dispatch(descriptionChange(currentProduct.description));
    //     dispatch(typeChange(currentProduct.type));
    //     dispatch(imageUrlChange(currentProduct.imageUrl));
    //     dispatch(buyPriceChange(currentProduct.buyPrice));
    //     dispatch(promotionPriceChange(currentProduct.promotionPrice));
    //     dispatch(amountChange(currentProduct.amount));
    //     dispatch(isPopularChange(currentProduct.isPopular));
    //     dispatch(colorChange(currentProduct.color));
    //     dispatch(subImageChange(currentProduct.subImage));
    // }

    const handleCloseAlert = () => {
        setOpenAlert(false);
    }

    const onBtnEditData = (paramOrderDetail) => {
        setCurrentOrderDetail(paramOrderDetail);
        setOpenModalDetailEdit(true);
    }
    const onBtnDeleteData = (paramOrderDetail) => {
        setCurrentOrderDetail(paramOrderDetail);
        setOpenModalDetailDelete(true);
    }


    const handleCancel = () => {
        handleCloseDetail();
    }
    const onBtnAddDataClick = () => {
        setOpenModalDetailAdd(true);

    }
    useEffect(() => {
        if(openModalDetail === true){
           dispatch(getAllProduct());
           dispatch(getAllOrderDetail(orderId));
        }
    }, [openModalDetail, varRefeshPage]);

    return (
        <>
            <Modal
                open={openModalDetail}
                onClose={handleCloseDetail}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Order Detail!</strong>
                    </Typography>
                    <Button onClick={onBtnAddDataClick} className="mb-2" value="add-user" style={{ borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px" }} variant="contained">Add Order Detail +</Button>
                    {orderDetails.map((row) => (
                        <Row key={row._id}>
                        <Col sm="5">
                            <Row className="mt-3">
                                <Col sm="3">
                                    <label>Product</label>
                                </Col>
                                <Col sm="9">
                                    <Input
                                        fullWidth
                                    value={products ? filterItems(products, row.product) : ""}
                                    // onChange={onChangeQuantity}
                                    // type="number"


                                    />
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="5">
                            <Row className="mt-3">
                                <Col sm="3">
                                    <label>Quantity</label>
                                </Col>
                                <Col sm="9">
                                    <Input
                                        fullWidth
                                    value={row.quantity}
                                    // onChange={onChangeQuantity}
                                    // type="number"
                                    />
                                </Col>
                            </Row>
                        </Col>
                        <Col sm="2">
                            <Row className="mt-3" style={{justifyContent: "flex-end"}}>
                            <Button className="mb-2" value="edit-order-detail" style={{ maxWidth: "50px", borderRadius: 10, backgroundColor: "#2196f3", padding: "10px 10px", fontSize: "10px" }} variant="contained" onClick={() => { onBtnEditData(row) }}>Edit</Button>
                            <Button className="mb-2" value="delete-order-detail" style={{ marginLeft: "10px", maxWidth: "50px", borderRadius: 10, backgroundColor: "#f50057", padding: "10px 10px", fontSize: "10px" }} onClick={() => { onBtnDeleteData(row) }}variant="contained">Delete</Button>
                            </Row>
                        </Col>
                        </Row>     
                    ))}
                  
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="6">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleCancel}>Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <OrderDetailAddModal varRefeshPage={varRefeshPage} setVarRefeshPage = {setVarRefeshPage} orderId={orderId} openModalDetailAdd={openModalDetailAdd} style={style} handleCloseDetailAdd={handleCloseDetailAdd} />
            <OrderDetailEditModal currentOrderDetail={currentOrderDetail}varRefeshPage={varRefeshPage} setVarRefeshPage = {setVarRefeshPage} openModalDetailEdit={openModalDetailEdit} style={style} handleCloseDetailEdit={handleCloseDetailEdit} />
            <OrderDetailDeleteModal openModalDetailDelete={openModalDetailDelete} orderId={orderId} currentOrderDetail={currentOrderDetail} style={style} handleCloseDetailDelete={handleCloseDetailDelete}/> 
        </>
    )
}
export default OrderDetailModal

