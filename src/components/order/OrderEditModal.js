import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
    costChange,
    noteChange
 } from "../../actions/inputOrderChangeAction";
import { editOrderAction } from "src/actions/editOrderAction";
import { getAllOrder } from "src/actions/getOrderAction";
function OrderEditModal({ style, openModalEdit, handleCloseEdit, currentOrder }) {
    const dispatch = useDispatch();
    const { name, imageUrl, description } = useSelector((reduxData) => reduxData.inputChangeReducer);
    const { cost, note } = useSelector((reduxData) => reduxData.inputOrderChangeReducer);
    const { error } = useSelector((reduxData) => reduxData.editProductTypeReducer);
    // const convertDate = (paramDate) => {
    //     let date =
    //     paramDate.getDate() +
    //   "/" +
    //   parseInt(paramDate.getMonth() + 1) +
    //   "/" +
    //   paramDate.getFullYear();
    //   return date;
    // }

    // const onNameChange = (event) => {
    //     dispatch(nameChange(event.target.value));
    // }
    // const onImageUrlChange = (event) => {
    //     dispatch(imageUrlChange(event.target.value));
    // }
    // const onDescriptionChange = (event) => {
    //     dispatch(descriptionChange(event.target.value));
    // }
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error");
    const [varRefeshPage, setVarRefeshPage] = useState(0);

    // const loadDataToInput = () => {
    //     dispatch(nameChange(currentProductType.name));
    //     dispatch(imageUrlChange(currentProductType.imageUrl));
    //     dispatch(descriptionChange(currentProductType.description));
    // }

    const handleCloseAlert = () => {
        setOpenAlert(false);
    }


    const handleCancel = (e) => {
        e.preventDefault()
        handleCloseEdit();
    };
    const handleEdit = () => {

        var updateData = {
            note: note,
            cost: cost,
        }
        var validateData = validateUpdateData(updateData);
        if (validateData) {
            dispatch(editOrderAction(currentOrder._id, updateData))
            if (error) {
              setOpenAlert(true)
              setStatusModal('error');
              setNoidungAlertValid('Edit order fail');
            } else {
              setOpenAlert(true)
              setStatusModal('success')
              setNoidungAlertValid('Edit order success');
              setVarRefeshPage(varRefeshPage + 1);
              handleCloseEdit();
            }
        
        }
    }

    const validateUpdateData = (paramOrder) => {
        if (paramOrder.cost === 0) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Cost is invalid!");
            return false;
        }
         else {
            return true;
        }

    }

    const onChangeCost = (event) => {
        dispatch(costChange(event.target.value))
    }

    const onChangeNote= (event) => {
        dispatch(noteChange(event.target.value))
    }

    // useEffect(() => {
    //     loadDataToInput();
    // }, [openModalEdit]);

    useEffect(() => {
        dispatch(getAllOrder());
    }, [varRefeshPage]);

    return (
        <>
            <Modal
                open={openModalEdit}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Edit Order!</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">

                        <Row className="mt-3">
                                <Col sm="5" >
                                    <label>Order Date </label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={currentOrder.orderDate?.slice(0, 10)}
                                        disabled
                                        // onChange={onChangeNote}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col sm="5" >
                                    <label>Ship Date </label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={currentOrder.shippedDate?.slice(0, 10)}
                                        disabled
                                        // onChange={onChangeNote}
                                    />
                                </Col>
                            </Row>

                        </Col>
                        <Col sm="6">
                        <Row className="mt-3">
                                <Col sm="5">
                                    <label>Cost</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        type = "number"
                                    value={cost}
                                    onChange={onChangeCost}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col sm="5" >
                                    <label>Note </label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={note}
                                        onChange={onChangeNote}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="6">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleEdit}>Edit Data</Button>
                                </Col>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleCancel}>Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
            {noidungAlertValid}
            </Alert>
        </Snackbar>

        </>
    )
}
export default OrderEditModal

