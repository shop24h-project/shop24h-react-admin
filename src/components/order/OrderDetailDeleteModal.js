import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deleteOrderDetailAction } from '../../actions/deleteOrderDetailAction';
import { getAllOrderDetail } from "src/actions/getOrderDetailAction";
function OrderDetailDeleteModal ({currentOrderDetail, style, openModalDetailDelete, handleCloseDetailDelete, orderId}) {
    const dispatch = useDispatch();
    const [openAlert, setOpenAlert] = useState(false)
    const [statusModalDelete, setStatusModalDelete] = useState("error")
    const [noidungAlert, setNoidungAlert] = useState("");
    
    // const { currentProductType } = useSelector((reduxData) => reduxData.getProductTypeReducer);
    const { error } = useSelector((reduxData) => reduxData.deleteOrderDetailReducer);
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    const onBtnCancelClick = () => {
        handleCloseDetailDelete()
    }
    const onBtnConfirmDeleteClick = () => {
        dispatch(deleteOrderDetailAction(orderId, currentOrderDetail._id))
    if (error) {
      setOpenAlert(true)
      handleCloseDetailDelete()
      setStatusModalDelete('error')
      setNoidungAlert('Delete order detail fail')
      setVarRefeshPage(varRefeshPage + 1);
    } else {
      setOpenAlert(true)
      setStatusModalDelete('success')
      handleCloseDetailDelete()
      setNoidungAlert('Delete order detail success')
      setVarRefeshPage(varRefeshPage + 1);
    }

    }

    
  useEffect(() => {
    dispatch(getAllOrderDetail(orderId));
  }, [varRefeshPage]);
    return (
        <>
            <Modal
            open={openModalDetailDelete}
            onClose={handleCloseDetailDelete}
            aria-labelledby="modal-delete"
            aria-describedby="modal-delete-user"
            >
            <Box sx={style}>
                <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                    <strong>Delete Order Detail!</strong>
                </Typography>
                    <Row className="mt-2">
                        <Col sm="12">
                            <p>Do you want to delete ?</p>
                        </Col>
                    </Row>
                    <Row className="mt-4 text-center">
                        <Col sm="12">
                            <Row className="mt-4">
                                <Col sm="6">
                                    <Button onClick={onBtnConfirmDeleteClick} className="bg-danger w-100 text-white">Xác nhận</Button>
                                </Col>
                                <Col sm="6">
                                    <Button onClick={onBtnCancelClick} className="bg-success w-75 text-white">Hủy Bỏ</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
            </Box>
        </Modal>
        <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={statusModalDelete} sx={{ width: '100%' }}>
            {noidungAlert}
            </Alert>
      </Snackbar>
        </>
    )
}

export default OrderDetailDeleteModal

