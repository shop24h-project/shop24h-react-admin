import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProduct } from "../../actions/getProductAction";
import {
    productChange,
    quantityChange
 } from "../../actions/inputOrderDetailChangeAction";
import { createOrdeDetailAction } from "src/actions/createOrderDetailAction";
import { getAllOrderDetail } from "src/actions/getOrderDetailAction";
function OrderDetailAddModal({varRefeshPage, setVarRefeshPage, orderId, style, openModalDetailAdd, handleCloseDetailAdd, customers }) {
    const dispatch = useDispatch();
    const { product, quantity } = useSelector((reduxData) => reduxData.inputOrderDetailChangeReducer);
    const { products } = useSelector((reduxData) => reduxData.getProductReducer);
    const { error } = useSelector((reduxData) => reduxData.createOrderReducer);
    const [openAlert, setOpenAlert] = useState(false);
    const [noidungAlertValid, setNoidungAlertValid] = useState("");
    const handleCloseAlert = () => {
        setOpenAlert(false);
    }
    const [statusModal, setStatusModal] = useState("error");
    const colorArr = [
        { value: "red", name: "Red" },
        { value: "green", name: "Green" },
        { value: "purple", name: "Purple" },
        { value: "blue", name: "Blue" },
        { value: "white", name: "White" }
    ]

    const onChangeProduct = (event) => {
        dispatch(productChange(event.target.value))
    }

    const onChangeQuantity = (event) => {
        dispatch(quantityChange(event.target.value))
    }
    const handleCancel = () =>{
        handleCloseDetailAdd();
    }
    const handleCreateData = () => {
        let today = new Date();
        var newData = {
            product: product,
            quantity: quantity
        }
        console.log(orderId);
        var validateData = validateNewData(newData);
     
        if (validateData) {
            dispatch(createOrdeDetailAction(orderId, newData));
 
            if (!error) {
                setOpenAlert(true);
                setStatusModal("success")
                setNoidungAlertValid("Add data success!")
                setVarRefeshPage(varRefeshPage + 1);
                handleCloseDetailAdd();
                // clearInput();
            } else {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Add data failed!")
                console.log(error);
            }
        }
    }
    const validateNewData = (paramNewData) => {
        if (paramNewData.product === "none") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Product is required!");
            return false;
        }
        if (paramNewData.quantity === 0) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Quantity is invalid!");
            return false;
        }
         else {
            return true;
        }

    }
    useEffect(() => {
        dispatch(getAllProduct());
      }, [openModalDetailAdd]);

      useEffect(() => {
        dispatch(getAllOrderDetail(orderId));
      }, [varRefeshPage]);
    return (
        <>
               <Modal
                open={openModalDetailAdd}
                onClose={handleCloseDetailAdd}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Add new Order Detail</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Product</label>
                                </Col>
                                <Col sm="7">
                                <Select
                                        MenuProps={{
                                            disableScrollLock: true,
                                        }}
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={product}
                                        onChange={onChangeProduct}
                                        style={{ width: "100%" }}
                                    >
                                        <MenuItem value="none">Select Product Type</MenuItem>
                                        {products ?
                                            products.map((row) => (
                                                <MenuItem key={row._id} value={row._id}>{row.name}</MenuItem>
                                            ))
                                            :
                                            ""
                                        }
                                    </Select>
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Quantity </label>
                                </Col>
                                <Col sm="7">
                                <Input
                                    fullWidth
                                    value={quantity}
                                    onChange={onChangeQuantity}
                                    type="number"
                                    />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="7">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleCreateData}>Create Order Detail</Button>
                                </Col> 
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleCancel}>Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>

        </>
    )
}
export default OrderDetailAddModal

