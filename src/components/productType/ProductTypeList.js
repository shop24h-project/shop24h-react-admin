
import { Container, Button, Grid, TableContainer, Paper, Table, TableHead, TableCell, TableRow, TableBody, Pagination } from "@mui/material";
import { getAllProductType } from "../../actions/getProductTypeAction";
import { useDispatch, useSelector } from "react-redux";
import CreateProductTypeModal from "./CreateProductTypeModal";
import React, { useEffect, useState } from "react";
import { getProductTypeById } from "../../actions/getProductTypeAction";
import DeleteProductTypeModal from "./DeleteProductTypeModal";
import EditProductTypeModal from "./EditProductTypeModal";
const ProductTypeList = () => {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4
  };
  const dispatch = useDispatch();
  const { productTypes } = useSelector((reduxData) => reduxData.getProductTypeReducer);
  const [varRefeshPage, setVarRefeshPage] = useState(0);
  const [openModalAdd, setOpenModalAdd] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const handleClose = () => setOpenModalAdd(false);
  const onBtnAddOrderClick = () => {
    setOpenModalAdd(true)
  }
  const [productTypeId, setProductTypeId] = useState('');
  const [currentProductType, setCurrentProductType] = useState('');
  const [filterKey, setFilterKey] = useState('');
  const [openModalDelete, setOpenModalDelete] = useState(false);



  const handleCloseDelete = () => setOpenModalDelete(false);
  const handleCloseEdit = () => setOpenModalEdit(false);
  const onBtnDeleteClick = (paramProductTypeId) => {
    setProductTypeId(paramProductTypeId);
    console.log(productTypeId);
    setOpenModalDelete(true);
  }
  const onBtnEditClick = (paramProductType) => {
    setCurrentProductType(paramProductType);
    setOpenModalEdit(true);
}
const handleFilter = (event) => {
  setFilterKey(event.target.value)
  console.log(filterKey)
}



useEffect(() => {
  dispatch(getAllProductType(filterKey));
}, [varRefeshPage, filterKey]);


  return (
    <div>
      <Container>
        <Button onClick={onBtnAddOrderClick} openModalAdd={openModalAdd} handleClose={handleClose} className="mb-2" value="add-user" style={{ borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px" }} variant="contained">Add ProductType +</Button>
        <input
              type="text"
              className="form-input-filter"
              style={{ padding: "5px 10px", float: "right"}}
              onChange={handleFilter}
              placeholder="Search.."
              // onChange={(e) => setFil(e.target.value)}
            />

        <Grid item style={{ width: "100%" }} >
          <TableContainer component={Paper} >
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Name </TableCell>
                  <TableCell align="center">Picture</TableCell>
                  <TableCell align="center">Description</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {productTypes.map((row) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell align="center" component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="center"><img style={{ width: "100px" }} src={row.imageUrl} /></TableCell>
                    <TableCell align="left">{row.description}</TableCell>
                    <TableCell align="center">
                      <Button style={{ borderRadius: 25, backgroundColor: "#2196f3", padding: "10px 20px", fontSize: "10px" }} variant="contained" onClick={() => onBtnEditClick((row))}>Edit</Button>
                      <Button style={{ borderRadius: 25, backgroundColor: "#f50057", marginLeft: "8px", padding: "10px 20px", fontSize: "10px" }} onClick={() => { onBtnDeleteClick((row._id)) }} variant="contained">Delete</Button>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        {/* <!-- Pagination --> */}
        {/* <Grid item xs={12} md={12} sm={12} lg={12} style={{marginTop: "10px"}}>
          <Grid container justifyContent={{ xs: "center", sm: "flex-end" }} >
            <Pagination
              count={noPage}
              defaultPage={1}
              onChange={onChangePagination}
              component="div"
            />
          </Grid>
        </Grid> */}
        <CreateProductTypeModal style={style} varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} setOpenModalAdd={setOpenModalAdd} openModalAdd={openModalAdd} handleClose={handleClose} />
        <EditProductTypeModal  currentProductType = {currentProductType} setOpenModalEdit={setOpenModalEdit} openModalEdit={openModalEdit} style={style} handleCloseEdit={handleCloseEdit}/>
         <DeleteProductTypeModal  productTypeId={productTypeId} openModalDelete={openModalDelete} style={style} handleCloseDelete={handleCloseDelete}/> 
      </Container>
    </div>
  )
}

export default ProductTypeList
