import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { createProductTypeAction } from '../../actions/createProductTypeAction';




function CreateProductTypeModal({ style, openModalAdd, setOpenModalAdd, handleClose, varRefeshPage, setVarRefeshPage }) {
    const dispatch = useDispatch();
    const [name, setName] = useState("");
    const [imageUrl, setImageUrl] = useState("");
    const [description, setDescription] = useState("");
    const { error } = useSelector((reduxData) => reduxData.createProductTypeReducer);
    const onNameChange = (event) => {
        setName(event.target.value)
    }
    const onImageUrlChange = (event) => {
        setImageUrl(event.target.value)
    }
    const onDescriptionChange = (event) => {
        setDescription(event.target.value)
    }
    const onBtnCancelClick = () => {
        handleClose();
    }
    const clearInput = () => {
        setName("");
        setImageUrl("");
        setDescription("");
    }

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error")

    const handleCloseAlert = () => {
        setOpenAlert(false)
    }



    const onBtnCreateDataClick = () => {
        var newData = {
            name: name,
            imageUrl: imageUrl,
            description: description,
        }
        var validateData = validateNewData(newData);
        if (validateData) {
            dispatch(createProductTypeAction(newData));
            console.log(newData);
            if (!error) {
                setOpenAlert(true);
                console.log("asss")
                setStatusModal("success")
                setNoidungAlertValid("Add data success!")
                setVarRefeshPage(varRefeshPage + 1);
                handleClose();
                clearInput();
            } else {
                setOpenAlert(true);
                setStatusModal("error")
                setNoidungAlertValid("Add data failed!")
                console.log(error);
            }
        }
    }
    const validateNewData = (paramNewData) => {
        if (paramNewData.name === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Name is required!");
            return false;
        }
        if (paramNewData.imageUrl === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("ImageUrl is required!");
            return false;
        }
        if (paramNewData.description === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Description is required!");
            return false;
        } else {
            return true;
        }

    }

    return (
        <>
            <Modal
                open={openModalAdd}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Thêm User!</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">

                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Name</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        onChange={onNameChange}
                                        value={name}
                                    // style = {{ background: '#f0f0f0'}}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>imageUrl</label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={imageUrl}
                                        onChange={onImageUrlChange}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>

                        </Col>
                        <Col sm="6">
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Description </label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={description}
                                        onChange={onDescriptionChange}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="6">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={onBtnCreateDataClick} >Create Order</Button>
                                </Col>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={onBtnCancelClick} >Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>
        </>
    )
}
export default CreateProductTypeModal
