import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllProductType } from "../../actions/getProductTypeAction";
import { editProductTypeAction } from "../../actions/editProductTypeAction";
import {
    nameChange,
    imageUrlChange,
    descriptionChange,
} from "../../actions/inputChangeActions";
function EditProductTypeModal({ style, openModalEdit, handleCloseEdit, currentProductType }) {
    const dispatch = useDispatch();
    const { name, imageUrl, description } = useSelector((reduxData) => reduxData.inputChangeReducer);


    const { error } = useSelector((reduxData) => reduxData.editProductTypeReducer);


    const onNameChange = (event) => {
        dispatch(nameChange(event.target.value));
    }
    const onImageUrlChange = (event) => {
        dispatch(imageUrlChange(event.target.value));
    }
    const onDescriptionChange = (event) => {
        dispatch(descriptionChange(event.target.value));
    }
    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error");
    const [varRefeshPage, setVarRefeshPage] = useState(0);

    const loadDataToInput = () => {
        dispatch(nameChange(currentProductType.name));
        dispatch(imageUrlChange(currentProductType.imageUrl));
        dispatch(descriptionChange(currentProductType.description));
    }

    const handleCloseAlert = () => {
        setOpenAlert(false);
    }


    const handleCancel = (e) => {
        e.preventDefault()
        handleCloseEdit();
    };
    const handleEdit = () => {

        var dataProductType = {
            name: name,
            imageUrl: imageUrl,
            description: description,
        }
        var validateData = validateProductTypeData(dataProductType);
        if (validateData) {
            dispatch(editProductTypeAction(currentProductType._id, dataProductType))
            if (error) {
              setOpenAlert(true)
              setStatusModal('error');
              setNoidungAlertValid('Confirm order fail');
            } else {
              setOpenAlert(true)
              setStatusModal('success')
              setNoidungAlertValid('Confirm order success');
              setVarRefeshPage(varRefeshPage + 1);
              handleCloseEdit();
            }
        
        }
    }

    const validateProductTypeData = (paramProductType) => {
        if (paramProductType.name === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Name is required");
            return false;
        }


        if (paramProductType.imageUrl === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("imageUrl is required");
            return false;
        }
        else {
            return true;
        }

    }



    useEffect(() => {
        loadDataToInput();
    }, [openModalEdit]);

    useEffect(() => {
        dispatch(getAllProductType());
    }, [varRefeshPage]);

    return (
        <>
            <Modal
                open={openModalEdit}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Edit Product Type!</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">

                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Name</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        onChange={onNameChange}
                                        value={name}
                                    // style = {{ background: '#f0f0f0'}}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>imageUrl</label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={imageUrl}
                                        onChange={onImageUrlChange}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>

                        </Col>
                        <Col sm="6">
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Description </label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                        value={description}
                                        onChange={onDescriptionChange}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="6">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleEdit}>Edit Data</Button>
                                </Col>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleCancel}>Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
            {noidungAlertValid}
            </Alert>
        </Snackbar>

        </>
    )
}
export default EditProductTypeModal

