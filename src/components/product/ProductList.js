
import { Container, Button, Grid, TableContainer, Paper, Table, TableHead, TableCell, TableRow, TableBody, Pagination } from "@mui/material";
import { getAllProduct, changePagePagination } from "../../actions/getProductAction";
import { useDispatch, useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import DeleteProductModal from "./DeleteProductModal";
import CreateProductModal from "./CreateProductModal";
import EditProductModal from "./EditProductModal";
const ProductList = () => {
  const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4
  };
  const dispatch = useDispatch();
  const { countAll, currentPage, products, noPage } = useSelector((reduxData) => reduxData.getProductReducer);

  const [openModalAdd, setOpenModalAdd] = useState(false);
  const [productId, setProductId]= useState(0);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [currentProduct, setCurrentProduct]= useState(0);
  const [openModalDelete, setOpenModalDelete] = useState(false);
  const [varRefeshPage, setVarRefeshPage] = useState(0);
  const [filterKey, setFilterKey] = useState('');
  const handleCloseDelete = () => setOpenModalDelete(false);
  const handleClose = () => setOpenModalAdd(false);
  const handleCloseEdit = () => setOpenModalEdit(false);
  const onBtnAddProductClick = () => {
    setOpenModalAdd(true)
  }
  useEffect(() => {
    dispatch(getAllProduct(filterKey));
  }, [currentPage, filterKey, varRefeshPage]);
  const onBtnDeleteClick = (paramProductId) => {
    setProductId(paramProductId);
    setOpenModalDelete(true)
  }
  const onBtnEditClick = (paramProduct) => {
    setCurrentProduct(paramProduct);
    setOpenModalEdit(true);
}
  const onChangePagination = (event, value) => {
    dispatch(changePagePagination(value));
  }
  const handleFilter = (event) => {
    setFilterKey(event.target.value)
  }
  
  return (
    <div>
      <Container>
      <Button onClick={onBtnAddProductClick} openModalAdd={openModalAdd} handleClose={handleClose} className="mb-2" value="add-user" style={{ borderRadius: 10, backgroundColor: "#689f38", padding: "10px 20px", fontSize: "10px" }} variant="contained">Add Product +</Button>
      <input
              type="text"
              className="form-input-filter"
              style={{ padding: "5px 10px", float: "right"}}
              onChange={handleFilter}
              placeholder="Search.."
            />
        <Grid item style={{ width: "100%" }} >
          <TableContainer component={Paper} >
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">Name </TableCell>
                  <TableCell align="center">description</TableCell>
                  <TableCell align="center">Picture</TableCell>
                  <TableCell align="center">Sub Picture</TableCell>
                  <TableCell align="center">Buy Price</TableCell>
                  <TableCell align="center">Promotion Price</TableCell>
                  <TableCell align="center">Amount</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {products.map((row,index) => (
                  <TableRow
                    key={row._id}
                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {row.name}
                    </TableCell>
                    <TableCell align="left">{row.description}</TableCell>
                    <TableCell align="center"><img style={{width: "100%"}}src = {row.imageUrl}/></TableCell>
                    <TableCell align="center"><img style={{width: "100%"}}src = {row.subImage}/></TableCell>
                    <TableCell align="center">{row.buyPrice}</TableCell>
                    <TableCell align="center">{row.promotionPrice}</TableCell>
                    <TableCell align="center">{row.amount}</TableCell>
                    <TableCell align="center">
                      <Button onClick={() => { onBtnEditClick((row)) }}style={{ borderRadius: 25, backgroundColor: "#2196f3", marginTop: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained">Sửa</Button>
                      <Button onClick={() => { onBtnDeleteClick((row._id)) }} style={{ borderRadius: 25, backgroundColor: "#f50057", marginTop: "8px", padding: "10px 20px", fontSize: "10px" }} variant="contained">Xóa</Button>
                    </TableCell>

                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
        {/* <!-- Pagination --> */}
        <Grid item xs={12} md={12} sm={12} lg={12} style={{marginTop: "10px"}}>
          <Grid container justifyContent={{ xs: "center", sm: "flex-end" }} >
            <Pagination
              count={noPage}
              defaultPage={1}
              onChange={onChangePagination}
              component="div"
            />
          </Grid>
        </Grid>
        <CreateProductModal style={style} varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} setOpenModalAdd={setOpenModalAdd} openModalAdd={openModalAdd} handleClose={handleClose} />
        <DeleteProductModal productId={productId} openModalDelete={openModalDelete} style={style} handleCloseDelete={handleCloseDelete}/>
        <EditProductModal currentProduct={currentProduct} setOpenModalEdit={setOpenModalEdit} openModalEdit={openModalEdit} style={style} handleCloseEdit={handleCloseEdit}/>
      </Container>
    </div>
  )
}

export default ProductList
