import { Alert, Box, Button, fabClasses, FormControl, Input, MenuItem, Modal, Select, Snackbar, Typography, TextareaAutosize } from "@mui/material"
import { Col, Row } from "reactstrap"
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllCustomter } from "../../actions/getCustomerAction";
import { editProductAction } from "../../actions/editProductAction";
import { getAllProductType } from '../../actions/getProductTypeAction';
import {
    nameChange,
    descriptionChange,
    typeChange,
    imageUrlChange,
    buyPriceChange,
    promotionPriceChange,
    amountChange,
    isPopularChange,
    colorChange,
    subImageChange
} from "../../actions/inputProductChangeAction";
import { getAllProduct } from "src/actions/getProductAction";
function EditProductModal({ style, openModalEdit, handleCloseEdit, currentProduct }) {
    const dispatch = useDispatch();
    const { name, description, type, imageUrl, buyPrice, promotionPrice, amount, isPopular, color, subImage } = useSelector((reduxData) => reduxData.inputProductChangeReducer);

    const { error } = useSelector((reduxData) => reduxData.editProductReducer);
    const { productTypes } = useSelector((reduxData) => reduxData.getProductTypeReducer);

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [statusModal, setStatusModal] = useState("error");
    const [varRefeshPage, setVarRefeshPage] = useState(0);
    const colorArr = [
        { value: "red", name: "Red" },
        { value: "green", name: "Green" },
        { value: "purple", name: "Purple" },
        { value: "blue", name: "Blue" },
        { value: "white", name: "White" }
    ]

    const onNameChange = (event) => {
        dispatch(nameChange(event.target.value));
    }
    const onDescriptionChange = (event) => {
        dispatch(descriptionChange(event.target.value));
    }
    const onTypeChange = (event) => {
        dispatch(typeChange(event.target.value));
    }
    const onImageUrlChange = (event) => {
        dispatch(imageUrlChange(event.target.value));
    }
    const onBuyPriceChange = (event) => {
        dispatch(buyPriceChange(event.target.value));
    }
    const onPromotionPriceChange = (event) => {
        dispatch(promotionPriceChange(event.target.value));
    }

    const onAmountChange = (event) => {
        dispatch(amountChange(event.target.value));
    }
    const onIsPopularChange = (event) => {
        dispatch(isPopularChange(event.target.value));
    }
    const onColorChange = (event) => {
        dispatch(colorChange(event.target.value));
    }
    const onSubImageChange = (event) => {
        dispatch(subImageChange(event.target.value));
    }
    
    const loadDataToInput = () => {
        dispatch(nameChange(currentProduct.name));
        dispatch(descriptionChange(currentProduct.description));
        dispatch(typeChange(currentProduct.type));
        dispatch(imageUrlChange(currentProduct.imageUrl));
        dispatch(buyPriceChange(currentProduct.buyPrice));
        dispatch(promotionPriceChange(currentProduct.promotionPrice));
        dispatch(amountChange(currentProduct.amount));
        dispatch(isPopularChange(currentProduct.isPopular));
        dispatch(colorChange(currentProduct.color));
        dispatch(subImageChange(currentProduct.subImage));
    }

    const handleCloseAlert = () => {
        setOpenAlert(false);
    }


    const handleCancel = (e) => {
        e.preventDefault()
        handleCloseEdit();
    };
    const handleEdit = () => {

        var dataProduct = {
            name: name,
            description: description,
            type: type,
            imageUrl: imageUrl,
            buyPrice: buyPrice,
            promotionPrice: promotionPrice,
            amount: amount,
            isPopular: isPopular,
            color: color,
            subImage: subImage,
        }
        var validateData = validateUpdateData(dataProduct);
        if (validateData) {
            dispatch(editProductAction(currentProduct._id, dataProduct))
            if (error) {
              setOpenAlert(true)
              setStatusModal('error');
              setNoidungAlertValid('Edit product fail');
            } else {
              setOpenAlert(true)
              setStatusModal('success')
              setNoidungAlertValid('Edit product success');
              setVarRefeshPage(varRefeshPage + 1);
              handleCloseEdit();
            }
        
        }
    }

    const validateUpdateData = (paramUpdateData) => {
        if (paramUpdateData.name === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Name is required!");
            return false;
        }
        if (paramUpdateData.type === "none") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Product Type is required!");
            return false;
        }

        if (paramUpdateData.imageUrl === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Image Url is required!");
            return false;
        } 
        if (paramUpdateData.buyPrice <= 0) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Buy Price is invalid!");
            return false;
        } 
        if (paramUpdateData.promotionPrice <= 0) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Promotion Price is invalid!");
            return false;
        } 
        if (paramUpdateData.amount < 0) {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Amount is invalid!");
            return false;
        } 
        if (paramUpdateData.subImage === "") {
            setOpenAlert(true);
            setStatusModal("error")
            setNoidungAlertValid("Sub Image is required!");
            return false;
        }

         else {
            return true;
        }

    }


    useEffect(() => {
        loadDataToInput();
    }, [openModalEdit]);

    useEffect(() => {
        dispatch(getAllProduct());
      }, [varRefeshPage]);
    return (
        <>
               <Modal
                open={openModalEdit}
                onClose={handleCloseEdit}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Typography mb={2} id="modal-modal-title" variant="h5" component="h2">
                        <strong>Edit Product!</strong>
                    </Typography>
                    <Row>
                        <Col sm="6">

                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Name</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                    onChange={onNameChange}
                                    value={name}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Description </label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                    value={description}
                                    onChange={onDescriptionChange}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Product Type</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        MenuProps={{
                                            disableScrollLock: true,
                                        }}
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={type}
                                        onChange={onTypeChange}
                                        style={{ width: "100%" }}
                                        disableScrollLock={true}
                                    >
                                        <MenuItem value="none">Select Product Type</MenuItem>
                                        {productTypes ?
                                            productTypes.map((row) => (
                                                <MenuItem key={row._id} value={row._id}>{row.name}</MenuItem>
                                            ))
                                            :
                                            ""
                                        }

                                    </Select>
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>ImageUrl </label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                    value={imageUrl}
                                    onChange={onImageUrlChange}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Buy Price</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        type="number"
                                    onChange={onBuyPriceChange}
                                    value={buyPrice}
                                    />
                                </Col>
                            </Row>
                      


                        </Col>
                        <Col sm="6">
                        <Row className="mt-3">
                                <Col sm="5">
                                    <label>Promotion Price</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        type="number"
                                    onChange={onPromotionPriceChange}
                                    value={promotionPrice}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-3">
                                <Col sm="5">
                                    <label>Amount</label>
                                </Col>
                                <Col sm="7">
                                    <Input
                                        fullWidth
                                        type="number"
                                    onChange={onAmountChange}
                                    value={amount}
                                    />
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Is Popular</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        MenuProps={{
                                            disableScrollLock: true,
                                        }}
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={isPopular}
                                        onChange={onIsPopularChange}
                                        style={{ width: "100%" }}
                                        disableScrollLock={true}
                                    >

                                        <MenuItem value="true">True</MenuItem>
                                        <MenuItem value="false">False</MenuItem>

                                    </Select>
                                </Col>
                            </Row>

                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Color</label>
                                </Col>
                                <Col sm="7">
                                    <Select
                                        MenuProps={{
                                            disableScrollLock: true,
                                        }}
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={color}
                                        onChange={onColorChange}
                                        style={{ width: "100%" }}
                                        disableScrollLock={true}
                                    >
                                        {colorArr ?
                                            colorArr.map((row) => (
                                                <MenuItem key={row.value} value={row.value}>{row.name}</MenuItem>
                                            ))
                                            :
                                            ""
                                        }

                                    </Select>
                                </Col>
                            </Row>
                            <Row className="mt-4">
                                <Col sm="5" >
                                    <label>Sub Image </label>
                                </Col>
                                <Col sm="7">
                                    <TextareaAutosize
                                        minRows={3}
                                        placeholder=""
                                        style={{ width: '100%' }}
                                    value={subImage}
                                    onChange={onSubImageChange}
                                    ></TextareaAutosize>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mt-4" style={{ justifyContent: "end" }}>
                        <Col sm="7">
                            <Row>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleEdit}>Edit Product</Button>
                                </Col>
                                <Col sm="6">
                                    <Button className="bg-success w-75 text-white" onClick={handleCancel} >Cancel</Button>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert onClose={handleCloseAlert} severity={statusModal} sx={{ width: '100%' }}>
                    {noidungAlertValid}
                </Alert>
            </Snackbar>

        </>
    )
}
export default EditProductModal

